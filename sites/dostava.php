<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet"> 
        <title>Dostava | Drvna industrija Drvić</title>
        <link rel="stylesheet" href="../style.css">
        <link rel="icon" href="https://findicons.com/files/icons/914/cemagraphics/256/truck.png">
        </head>

    <body class="bodyDostava">
        <div class="navbar">
            <a href="../homepage.html">NASLOVNA</a>
            <a href="drva.html">DRVA</a>
            <a href="dostava.php">DOSTAVA</a>
            <a href="kontakt.html" class="right">KONTAKT</a>
        </div>
        <div class="sporedniNaslov centriraj">Uvijek najbrže i najpovoljnije!</div>
        <h1 class="openSans dostavaNaslov">Izračun cijene dostave: </h1>
        <form class="dostavaTekst" action="dostava.php" method="get" id="ok">
            <input type="radio" id="hrast" name="vrstaDrva" value="hrast">
            <label for="hrast">hrast</label><br>
            <input type="radio" id="bukva" name="vrstaDrva" value="bukva">
            <label for="bukva">bukva</label><br>
        
            Unesite dužinu koju kupujete u metrima: <input type="number" name="duzina"><br>
            <input type="submit">
        </form>
        <div class="dostavaTekst">Ukupno morate platiti: </div>
        <?php
            
            if(!empty($_GET['duzina']) && !empty($_GET['vrstaDrva'])){
                if($_GET['vrstaDrva']=="hrast"){
                    izracunajCijenu($_GET['duzina'],150);}
                else if($_GET['vrstaDrva']=="bukva"){
                    izracunajCijenu($_GET['duzina'],250);
                }
            }
            else{
                echo '<p class="upozorenje dostavaTekst">Molimo unesite sve tražene veličine!</p>';
            }

            function izracunajCijenu($duzina, $cijenadrva){
                $cijena=$duzina*$cijenadrva;
                $cijenadostave=$cijena/10;
                $pdv=($cijenadostave+$cijena)/4;
                echo "<p class='dostavaTekst'>Cijena po dužini drveta: ",$cijena," HRK</p>";
                echo "<p class='dostavaTekst'>Cijena dostave: ",$cijenadostave," HRK</p>";
                echo "<p class='dostavaTekst'>PDV: ",$pdv," HRK";
                echo "<p class='dostavaTekst'><b>Ukupno: ",$cijena+$cijenadostave+$pdv," HRK</b></p>";
            }
            
        ?>
        <footer class="footer centriraj">2021. Krunoslav Petrik</footer>
    </body>
</html>